const {
    Router
} = require('express')
const ModelController = require('../controllers/model.controller')

const router = new Router();
router.get('/models', ModelController.findModels);
router.get('/models/:modelId', ModelController.findModel);
router.post('/models', ModelController.createModel);
router.put('/models/:modelId', ModelController.updateModel);
router.delete('/models/:modelId', ModelController.deleteModel);



module.exports = router;