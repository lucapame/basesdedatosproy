const {
    Router
} = require('express')
const MakerCntroller = require('../controllers/maker.controller')

const router = new Router();
router.get('/makers', MakerCntroller.findMakers);
router.get('/makers/:makerId', MakerCntroller.findMaker);
router.post('/makers', MakerCntroller.createMaker);
router.put('/makers/:makerId', MakerCntroller.updateMaker);
router.delete('/makers/:makerId', MakerCntroller.deleteMaker);



module.exports = router;