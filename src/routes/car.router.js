const {
    Router
} = require('express')
const CarControler = require('../controllers/car.controller')

const router = new Router();
router.get('/cars', CarControler.findCarss);
router.get('/cars/:carId', CarControler.findCar);
router.post('/cars', CarControler.createCar);
router.put('/cars/:carId', CarControler.updateCar);
router.delete('/cars/:carId', CarControler.deleteCar);



module.exports = router;