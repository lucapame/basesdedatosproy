const jwt = require('jsonwebtoken')
const config = require('config');

/**
 * Generates an auth middleware to use in a route.
 * If `roles` are specified, the middleware will allow access
 * only if the role of the user in the token is contained.
 * @param {string[]} roles allowed to access
 */
const auth = (roles) => (req, res, next) => {

    // Check if auth header exists
    const authHeader = req.header('Authorization');
    if (!authHeader) {
        return res.status(401).json({ errors: [{ msg: 'No token, authorization denied' }] });
    }

    //check if valid token format
    const token = authHeader.split(' ')[1];
    if (!token) {
        return res.status(401).json({ errors: [{ msg: 'Wrong token format, authorization denied' }] });
    }

    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        const role = decoded.user.role;
        if (roles && !roles.includes(role)) {
            // This means that the user is authenticated
            // But does not have permission to access this route
            return res.status(401).json({
                errors: [{ msg: `Insufficient role privileges (only: ${roles})` }] 
            })
        }
        req.user = decoded.user;
        next();
    } catch (err) {
        return res.status(401).json({ errors: [{ msg: 'Invalid token, authorization denied' }] })
    }
}

module.exports = auth;
