const mongoose = require('mongoose');
const {
    Schema
} = mongoose;


const MakerSchema = new Schema({

    maker: {
        type: String,
        required: true
    },
    fullname: {
        type: String,
        required: true,
    },

    location: {
        countryname: {
            type: String,
        },
        continent: {
            type: String,
        },

    },

}, {
    timestamps: false,
});


module.exports = mongoose.model('Maker', MakerSchema);