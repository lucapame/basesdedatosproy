const mongoose = require('mongoose');
const {
    Schema
} = mongoose;


const CarSchema = new Schema({
    model: {
        type: String,
        required: true
    },
    descr: {
        type: String,
        required: true,
    },

    details: {
        cylinders: {
            type: Number,
        },
        edispl: {
            type: Number,
        },
        weight: {
            type: Number,
        },
        accel: {
            type: Number,
        },
        year: {
            type: Number,
        },
        mpg: {
            type: Number,
        },
        horsepower: {
            type: Number,
        },
    },

}, {
    timestamps: false,
});


module.exports = mongoose.model('Car', CarSchema);