const mongoose = require('mongoose');
const {
    Schema
} = mongoose;

const ModelDetailsSchema = new Schema({

    maker: {
        type: Schema.Types.ObjectId,
        ref: 'Maker',
        required: true

    },
    model: {
        type: String,
        required: true,
    },


}, {
    timestamps: false,
});


module.exports = mongoose.model('Model_Details', ModelDetailsSchema);