const App = require('./app');
const Database = require('./database/db-connection');
const config = require('config');

const port = process.env.PORT || 5000;
const connectionString = config.get('mongoURI');

if (!connectionString) {
    throw new Error("No connection string setting in environment variables");
}

const app = new App(port, [
    // Routes go here
    require('./routes/car.router'),
    require('./routes/maker.router'),
    require('./routes/model_details.router'),
]);
const db = new Database(connectionString);

db.connect().then(() => {
    console.log(`Successfully connected to database`);
    app.listen().then(() => console.log(`magic hapens on ${port}`));

}).catch(error => {
    console.log(error);
});