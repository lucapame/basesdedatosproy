const {
    validationResult
} = require('express-validator')

const MakerService = require('../services/maker.service')

const findMakers = async (request, response) => {

    try {
        const makers = await MakerService.findMakers();
        return response.send(makers);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        return response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}



const findMaker = async (request, response) => {

    try {
        const makerId = request.params.makerId;
        const maker = await MakerService.findMaker(makerId);
        response.send(maker);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


const createMaker = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    const data = request.body;
    console.log(data)
    try {
        const maker = await MakerService.createMaker(data);
        return response.send(maker);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}

const updateMaker = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const makerId = request.params.makerId;
        const data = request.body;
        const maker = await MakerService.updateMaker(makerId, data);
        response.send(maker);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }


}

const deleteMaker = async (request, response) => {

    try {
        const id = request.params.makerId;
        await MakerService.deleteMaker(id);
        response.send();
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


module.exports = {
    findMakers,
    findMaker,
    createMaker,
    deleteMaker,
    updateMaker
};