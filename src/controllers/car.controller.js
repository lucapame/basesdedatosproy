const {
    validationResult
} = require('express-validator')

const CarService = require('../services/car.service')

const findCarss = async (request, response) => {

    try {
        const cars = await CarService.findCars();
        return response.send(cars);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        return response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}



const findCar = async (request, response) => {

    try {
        const carId = request.params.carId;
        const car = await CarService.findCar(carId);
        response.send(car);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


const createCar = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    const data = request.body;
    console.log(data)
    try {
        const car = await CarService.createCar(data);
        return response.send(car);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}

const updateCar = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const carId = request.params.carId;
        const data = request.body;
        const car = await CarService.updateCar(carId, data);
        response.send(car);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }


}

const deleteCar = async (request, response) => {

    try {
        const id = request.params.carId;
        await CarService.deleteCar(id);
        response.send();
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


module.exports = {
    findCarss,
    findCar,
    createCar,
    deleteCar,
    updateCar
};