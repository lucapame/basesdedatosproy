const {
    validationResult
} = require('express-validator')

const ModelService = require('../services/model_details.service')

const findModels = async (request, response) => {

    try {
        const models = await ModelService.findModels();
        return response.send(models);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        return response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}



const findModel = async (request, response) => {

    try {
        const modelId = request.params.modelId;
        const model = await ModelService.findModel(modelId);
        response.send(model);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


const createModel = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    const data = request.body;
    console.log(data)
    try {
        const model = await ModelService.createModel(data);
        return response.send(model);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}

const updateModel = async (request, response) => {
    const errors = validationResult(request);
    if (!errors.isEmpty()) {
        return response.status(400).json({
            errors: errors.array()
        })
    }

    try {
        const modelId = request.params.modelId;
        const data = request.body;
        const model = await ModelService.updateModel(modelId, data);
        response.send(model);
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }


}

const deleteModel = async (request, response) => {

    try {
        const id = request.params.modelId;
        await ModelService.deleteModel(id);
        response.send();
    } catch (err) {
        const errorCode = err.errorCode || 500;
        console.log(err);
        response.status(errorCode).json({
            errors: [{
                msg: err.message
            }]
        });
    }
}


module.exports = {
    findModels,
    findModel,
    createModel,
    deleteModel,
    updateModel
};