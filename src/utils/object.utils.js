const removeUndefinedAttributes = (object) => {
    for (let prop in object) {
        if (object[prop] === undefined)
        delete object[prop];
    }
    return object;
}

module.exports = { removeUndefinedAttributes };