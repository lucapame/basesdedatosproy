class CustomError extends Error {
    errorCode;
    constructor(errorCode, message) {
        super(message)
        this.errorCode = errorCode;
    }
}

module.exports = CustomError;