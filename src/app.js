const express = require('express');
const cors = require('cors');

class App {

    constructor(port, routes) {
        this.app = express();
        this.port = port;
        this.initializeMiddlewares();
        this.initializeRoutes(routes);
    }

    listen() {
        return new Promise((resolve, reject) => {
            this.app.listen(this.port, () => {
                resolve();
            }).on('error', (error) => {
                reject(error);
            })
        })
    }

    initializeMiddlewares() {
        this.app.use(express.json({
            extended: false
        }))
        this.app.use(cors());
    }

    initializeRoutes(routes) {
        routes.forEach((route) => {
            this.app.use('/api/', route);
        });
    }
}

module.exports = App;