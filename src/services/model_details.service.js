const Model = require('../models/model_details.model');
const {
    removeUndefinedAttributes
} = require('../utils/object.utils');


class ModelDetailsService {

    async findModels() {
        const models = await Model.find()
        return models;
    }


    async findModel(modelId) {
        const model = await Model.findById(modelId);
        if (!model) {
            throw new CustomError(404, 'model does not exists');
        }
        return model.toJSON()
    }



    async createModel(data) {
        const newmodel = new Model(data);
        return newmodel.save();

    }

    async updateModel(id, {
        maker,
        model,
    }) {
        let makerUpd = await Maker.findById(id);
        if (!makerUpd) {
            throw new CustomError(404, 'model does not exists');
        }

        let fields = {
            maker,
            model,
        };
        fields = removeUndefinedAttributes(fields);

        return Model.findByIdAndUpdate(
            id, fields, {
                new: true,
                useFindAndModify: false
            }
        )
    }

    async deleteModel(id) {
        let model = await Model.findById(id);
        if (!model) {
            throw new CustomError(404, 'model does not exists');
        }

        await Model.deleteOne({
            _id: id
        });

    }





}

module.exports = new ModelDetailsService();