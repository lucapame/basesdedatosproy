const Maker = require('../models/maker.model');
const {
    removeUndefinedAttributes
} = require('../utils/object.utils');


class MakerService {

    async findMakers() {
        const makers = await Maker.find()
        return makers;
    }


    async findMaker(makerId) {
        const maker = await Maker.findById(makerId);
        if (!maker) {
            throw new CustomError(404, 'maker does not exists');
        }
        return maker.toJSON()
    }



    async createMaker(data) {
        const newMaker = new Maker(data);
        return newMaker.save();

    }

    async updateMaker(id, {
        maker,
        fullname,
        location,
    }) {
        let makerUpd = await Maker.findById(id);
        if (!makerUpd) {
            throw new CustomError(404, 'maker does not exists');
        }

        let fields = {
            maker,
            fullname,
            location,
        };
        fields = removeUndefinedAttributes(fields);

        return Maker.findByIdAndUpdate(
            id, fields, {
                new: true,
                useFindAndModify: false
            }
        )
    }

    async deleteMaker(id) {
        let maker = await Maker.findById(id);
        if (!maker) {
            throw new CustomError(404, 'maker does not exists');
        }

        await Maker.deleteOne({
            _id: id
        });

    }





}

module.exports = new MakerService();