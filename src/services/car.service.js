const Car = require('../models/car.model');
const {
    removeUndefinedAttributes
} = require('../utils/object.utils');
const CustomError = require('../utils/CustomError')

class CarService {

    async findCars() {
        const cars = await Car.find()
        return cars;
    }


    async findCar(carId) {
        const car = await Car.findById(carId);
        if (!car) {
            throw new CustomError(404, 'car does not exists');
        }
        return car.toJSON()
    }



    async createCar(data) {
        const newCar = new Car(data);
        return newCar.save();

    }

    async updateCar(id, {
        model,
        descr,
        details,
    }) {
        let car = await Car.findById(id);
        if (!car) {
            throw new CustomError(404, 'car does not exists');
        }

        let fields = {
            model,
            descr,
            details,
        };
        fields = removeUndefinedAttributes(fields);

        return Car.findByIdAndUpdate(
            id, fields, {
                new: true,
                useFindAndModify: false
            }
        )
    }

    async deleteCar(id) {
        let car = await Car.findById(id);
        if (!car) {
            throw new CustomError(404, 'car does not exists');
        }

        await Car.deleteOne({
            _id: id
        });

    }





}

module.exports = new CarService();